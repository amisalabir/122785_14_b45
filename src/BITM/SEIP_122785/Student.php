<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/18/2017
 * Time: 2:21 PM
 */

namespace Tap;


use App\Person;

class Student extends Person
{
    private $StudentID;

    public function setStudentID($StudentID)
    {
        $this->StudentID = $StudentID;
    }

    public function getStudentID()
    {
        return $this->StudentID;
    }
}