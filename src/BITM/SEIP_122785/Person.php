<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/18/2017
 * Time: 2:20 PM
 */

namespace App;


class Person
{
    private $name;
    private $DateOfBirth;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDateOfBirth($DateOfBirth)
    {
        $this->DateOfBirth = $DateOfBirth;
    }

    public function getDateOfBirth()
    {
        return $this->DateOfBirth;
    }
}